import composer from './helpers/composer';
import Draft from './behaviors/draft';

const state = () => {
  return {
    name: '',
    message: ''
  }
}

const actions = {
  update({ commit }, note) {
    commit('update', note);
  }
}

const mutations = {
  update(state, note) {
    Object.assign(state, { ...note })
  }
}

export default composer({
  namespaced: true,
  state,
  actions,
  mutations
}, Draft());
