import Vue from 'vue';

const generateId = () => Math.random().toString(36).substring(7);

export default function({ initItem }) {
  return {

    state() {
      return {
        items: {},
        ids: []
      }
    },


    getters: {

      item: (state) => ({ id }) => state.items[id]

    },


    actions: {

      addItem({ commit }) {
        const id = generateId();
        const child = initItem();

        commit('addItem', { id, child });

        return id;
      },

      deleteItem({ commit }, id) {
        commit('deleteItem', { id });
      }
    },


    mutations: {

      addItem(state, { id, child } ) {
        Vue.set(state.items, id, child);
        state.ids.push(id);
      },

      deleteItem(state, { id }) {
        Vue.delete(state.items, id);
        const index = state.ids.indexOf(id);
        state.ids.splice(index, 1);
      }

    }
  }
}
