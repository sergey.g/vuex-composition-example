import Vue from 'vue'
import Vuex from 'vuex'

import notes from './noteList';

import api from '../api';

Vue.use(Vuex)

const store = new Vuex.Store({
  modules: {
    notes
  }
});

export default store;
