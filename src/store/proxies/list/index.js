import Vue from 'vue';

export default function() {

  return {

    getters(children) {
      return {
        
        // Map each children getter into a proxified closure with a scoped state.
        ...Object.entries(children).reduce((getters, [name, getter]) => ({
          ...getters,

          [name]: (parent_state) => ({ id, ...payload }) => {
            const state = parent_state.items[id];
            return getter(state);
          }
        }), {})
      }
    },

    actions(children) {

      return {

        // Map each children action into a proxified closure with a modified commit function.
        ...Object.entries(children).reduce((actions, [name, action]) => ({
          ...actions,

          [name]: ({ commit: parent_commit, state, ...rest }, { id, ...payload }) => {
            const commit = (reason, payload) => {
              parent_commit(reason, { id, ...payload });
            }

            return action({ commit, state, ...rest }, payload)
          }
        }), {})
      }
    },


    mutations(children) {

      return {

        // Map each children mutation into a new mutation that applies the original mutation on the item state referenced by the provided id
        ...Object.entries(children).reduce((mutations, [name, mutation]) => ({
          ...mutations,
          // The parent_state is not reused here, as the correct slice is passed as the item in the payload
          [name]: (parent_state, { id, ...payload }) => {
            const state = parent_state.items[id];
            mutation(state, payload)
          }
        }), {})
      }
    }
  }
}
