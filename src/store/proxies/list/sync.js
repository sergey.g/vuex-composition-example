const slashPath = (namespace) => namespace.join('/');
const getAction = (store, namespace) => store.dispatch.bind(null, slashPath(namespace));
const getGetter = (store, namespace) => store.getters[slashPath(namespace)];

export default function({ namespace, getId }) {

  return {

    // The syncActions and syncGetters functions take a list of names of actions
    // or getter, and returns an object to insert into the methods of a vue to
    // map vuex module actions and getters to methods.

    syncActions(names) {
      // For each of these names (the actual actions) ...
      return names.reduce((synced, name) => ({
        // it accumulates the previously created functions
        ...synced,
        // It creates the required getter function,
        // named after the given `name` in the list of names
        [name](payload) {
          const action = getAction(this.$store, [ ...namespace, name ])
          return action({ id: getId(this), ...payload });
        }
      }), {})
    },

    syncGetters(names) {
      // For each of these names (the actual getters) ...
      return names.reduce((synced, name) => ({
        // it accumulates the previously created functions
        ...synced,
        // It creates the required getter function,
        // named after the given `name` in the list of names
        [name](payload) {
          const getter = getGetter(this.$store, [ ...namespace, name ])
          return getter({ id: getId(this), ...payload });
        }
      }), {})
    }
  }


}
