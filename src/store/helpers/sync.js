export default function({ getState, setState }) {

  return {
    // The syncState function takes a list of name, and returns an object of computed properties.
    // Each named computed property provides :
    // - a getter that calls the 'getState' getter to returns the property corresponding to the given name, and
    // - a setter that calls the 'setState' action to propagate modification to vuex.
    syncState(names) {
      return names.reduce((synced, name) => {
        return {
          ...synced,
          [name]: {
            get() {
              return this[getState][name];
            },
            set(payload) {
              return this[setState]({ [name]: payload })
            }
          }
        }
      }, {})
    }

  }

}
